import * as Utils from '@shared/utils.js'
import Systems from './register.js'

window.setup = function () {
  createCanvas(640, 480).parent("cv")

  this.viewport = Utils.getViewport(this)
  this.keyboard = new Array(256)

  // call each system's onload method
  Systems.forEach(system => system.onload && system.onload())
}

window.draw = function () {
  background(0)

  // get the time
  let time = Utils.getTime(millis())

  // run each system's onframe method
  Systems.forEach(s => s.onframe && s.onframe(time))
}

window.keyPressed = function (key) {
  window.keyboard[key.keyCode] = true
  return true
}

window.keyReleased = function (key) {
  window.keyboard[key.keyCode] = false
  return true
}

window.pause = Utils.pause