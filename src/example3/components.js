export class AlienParticleGenerator { }

export class StarParticleGenerator { }

export class Particle { }

export class BombProjectile { }

export class Player {

  constructor () {
    this.timeUntilNextBullet = 0;
    this.timeUntilNextBomb = 0;
    this.alive = true;
  }

}

export class BulletProjectile { }

export class PlayerTrailParticleGenerator { }

export class Projectile {

  constructor (timeToLive) {
    // life time in ms
    this.timeToLive = timeToLive;
  }

}

export class ParticleLifeTime {

  constructor (timeToLive) {
    // life time in ms
    this.timeToLive = timeToLive;
  }

}

export class ParticleExplosion {

  constructor (maxLife, frameStarted) {
    this.maxLife = maxLife;
    this.frameStarted = frameStarted; //  current frame when the explosion started
  }

}

export class BombExplosion extends ParticleExplosion { }

export class ProjectileObstacle { }

export class PlayerObstacle { }

export class ShapeParticleEmitter {

  constructor (minMaxStartPos,
    minMaxVelocity,
    minRadius, maxRadius,
    minSides, maxSides,
    minFill, maxFill,
    minStroke, maxStroke) {
    // x1, x2, y1, y2
    this.minMaxStartPos = minMaxStartPos;

    // x1, x2, y1, y2, r1, r2
    this.minMaxVelocity = minMaxVelocity;

    this.minRadius = minRadius;
    this.maxRadius = maxRadius;

    this.minSides = minSides;
    this.maxSides = maxSides;

    this.minFill = minFill;
    this.maxFill = maxFill;

    this.minStroke = minStroke;
    this.maxStroke = maxStroke;
  }

}

export class Mesh {

  constructor (points, normals) {
    this.points = points;
    this.normals = normals;
  }

}

export class Movement {

  constructor (velocityX, velocityY, velocityR) {
    this.velocityX = velocityX;
    this.velocityY = velocityY;
    this.velocityR = velocityR;
  }

}

export class TimerParticleSpawner {

  constructor (rebirthRate, minLife, maxLife, elapsedMs) {
    // rebirth rate in ms
    this.rebirthRate = rebirthRate

    // life time in ms
    this.minLife = minLife
    this.maxLife = maxLife

    this.elapsedMs = elapsedMs
  }

}

export class AlienParticleSpawner extends TimerParticleSpawner { }

export class StarParticleSpawner extends TimerParticleSpawner { }

export class PlayerTrailParticleSpawner extends TimerParticleSpawner { }


export class Position {

  constructor (x, y) {
    this.x = x
    this.y = y
  }

}

export class Direction {

  constructor (dirX, dirY) {
    this.dirX = dirX
    this.dirY = dirY
  }

}

export class Shape {

  constructor (sides, rotation, radius, fill, stroke) {
    this.sides = sides
    this.rotation = rotation
    this.radius = radius
    this.fill = fill
    this.stroke = stroke
  }

}

export class Bomb {

}


export class PlayerScore {
  constructor () {
    this.total = 0
    this.timeDied = 0
  }
}
