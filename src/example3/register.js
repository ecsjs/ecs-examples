import { ecs } from 'ecsjs'
import * as Components from './components.js'

// Aliens
import { AlienParticleGeneratorSystem } from './systems/aliens/alien-particle-generator-system.js'
// Stars
import { StarParticleGeneratorSystem } from './systems/stars/star-particle-generator-system.js'
import { PlayerTrailGeneratorSystem } from './systems/player/player-trail-generator-system.js'
// Particles
import { ParticleLifeSystem } from './systems/particles/particle-lifetime-system.js'
import { ParticleMovementSystem } from './systems/particles/particle-movement-system.js'
// Player
import { PlayerSpawnSystem } from './systems/player/player-spawn-system'
import { PlayerMovementSystem } from './systems/player/player-movement-system.js'
// Projectiles
import { PlayerProjectileSpawnSystem } from './systems/player/player-projectile-spawn-system'
import { ProjectileMovementSystem } from './systems/projectiles/projectile-movement-system.js'
// Collisions
import { ProjectileCollisionSystem } from './systems/projectiles/projectile-collision-system.js'
import { BombExplosionCollisionSystem } from './systems/bombs/bomb-explosion-collision-system.js'
import { PlayerCollisionSystem } from './systems/player/player-collision-system.js'
// Explosions
import { BombExplosionSystem } from './systems/bombs/bomb-explosion-system.js'
import { ParticleExplosionSystem } from './systems/particles/particle-explosion-system.js'
// Audio
import { SoundSystem } from './systems/sound/sound-system.js'
// Draw
import { RenderSystem } from './systems/render-system.js'

// register components
ecs.register(...Object.values(Components))

// execution order
export default [
  AlienParticleGeneratorSystem,
  StarParticleGeneratorSystem,
  ParticleLifeSystem,
  ParticleMovementSystem,
  PlayerSpawnSystem,
  PlayerTrailGeneratorSystem,
  PlayerMovementSystem,
  PlayerProjectileSpawnSystem,
  ProjectileMovementSystem,
  ProjectileCollisionSystem,
  BombExplosionCollisionSystem,
  PlayerCollisionSystem,
  BombExplosionSystem,
  ParticleExplosionSystem,
  RenderSystem,
  SoundSystem,
].map(s => new s(window))