import BaseSystem from '@shared/systems/base-system.js'
const soundPath = {
  laser: 'sound/LaserShoot.wav',
  explosion: 'sound/Explosion.wav',
  bomb: 'sound/BombShoot.wav',
  bombExplosion: 'sound/BombExplosion.wav',
  playerExplosion: 'sound/PlayerExplosion.wav'
}

const sound = {}

export class SoundSystem extends BaseSystem {

  onload() {
    const keys = Object.keys(soundPath)
    keys.forEach(k => sound[k] = this.p5.loadSound(soundPath[k]))
  }

  static play(key, volume = 1) {
    const s = sound[key]
    s.setVolume(0.1)
    s.play()
  }

}