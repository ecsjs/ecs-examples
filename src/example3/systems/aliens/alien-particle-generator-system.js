import * as Components from '@example3/components.js';
import { ParticleFactory } from '@example3/systems/particles/particle-factory.js';
import { AbstractTimerParticleSystem } from '@shared/systems/abstract-timer-particle-system.js';
import { ecs } from 'ecsjs';
import s2d from 'simple-2d';

export class AlienParticleGeneratorSystem extends AbstractTimerParticleSystem {

  constructor (p5) {
    super(p5, Components.AlienParticleSpawner)
  }

  onload() {
    this.createAlienGenerator()
  }

  createAlienGenerator() {
    const viewport = this.p5.viewport

    const rebirthRate = 100,  // every 10th of a second
      minLife = 4000,     // in ms
      maxLife = 5000,     // in ms

      // engine component
      timerParticleSpawner = new Components.AlienParticleSpawner(
        // rebirth rate in ms
        rebirthRate,

        // life time in ms
        minLife, maxLife,
        // elapsedMs
        0
      ),

      // create a shape generator
      shapeEmitter = new Components.ShapeParticleEmitter(
        // minMaxStartPos
        {
          x1: viewport.x1,    // min x
          x2: viewport.x2,    // max x
          y1: viewport.y1,    // min y
          y2: viewport.y1     // max y
        },
        // minMaxVelocity
        {
          x1: -0.025,         // minVelocityX
          x2: 0.025,          // maxVelocityX
          y1: .1,             // minVelocityY
          y2: .1,             // maxVelocityY
          r1: -0.002,         // minVelocityR // rotation min
          r2: 0.002           // maxVelocityR  // rotation max
        },
        10, // minRadius
        20, // maxRadius
        4,  // minSides
        20, // maxSides
        new s2d.Rgba(255, 128, 0, 1),      // minFill
        new s2d.Rgba(255, 255, 0, 1),      // maxFill
        new s2d.Rgba(255, 255, 255, 1),    // minStroke
        new s2d.Rgba(255, 255, 255, 1)     // maxStroke
      )


    const entityId = ecs.getNextId()
    ecs.set(entityId, new Components.AlienParticleGenerator())
    ecs.set(entityId, timerParticleSpawner)
    ecs.set(entityId, shapeEmitter)
  }

  onParticleBirth(time, entityId, spawner) {
    this.spawnAlienParticle(
      spawner,
      ecs.get(entityId, Components.ShapeParticleEmitter)
    )
  }

  spawnAlienParticle(timeParticleSpawner, shapeEmitter) {
    // compute the life time
    const timeToLive = Math.round(s2d.randomMinMax(timeParticleSpawner.minLife, timeParticleSpawner.maxLife))

    // create and save the particle entity
    const entityId = ecs.getNextId()
    ecs.set(entityId, new Components.Particle())
    // set the life time
    ecs.set(entityId, new Components.ParticleLifeTime(timeToLive))
    // define this particle as a projectile obstacle
    ecs.set(entityId, new Components.ProjectileObstacle())
    // define this particle as a player obstacle
    ecs.set(entityId, new Components.PlayerObstacle())

    ParticleFactory.spawnShapeParticle(entityId, shapeEmitter)
  }

}