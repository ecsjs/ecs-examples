import * as Components from '@example3/components.js'
import BaseSystem from '@shared/systems/base-system.js'
import { ecs } from 'ecsjs'
import s2d from 'simple-2d'

export class PlayerMovementSystem extends BaseSystem {

  onframe(time) {
    const p5 = this.p5
    const viewport = this.p5.viewport
    const playerMap = ecs.getMap(Components.Player)
    const [entityId, player] = playerMap.firstEntry() ?? []

    if (!player || player.alive === false) return;

    const movement = ecs.get(entityId, Components.Movement)
    const position = ecs.get(entityId, Components.Position)
    const shape = ecs.get(entityId, Components.Shape)
    const mesh = ecs.get(entityId, Components.Mesh)

    let playerPosChanged = false;

    if (p5.keyboard[65]) {
      position.x -= movement.velocityX * time.delta;
      playerPosChanged = true
    } else if (p5.keyboard[68]) {
      position.x += movement.velocityX * time.delta;
      playerPosChanged = true
    }

    if (p5.keyboard[87]) {
      position.y -= movement.velocityY * time.delta;
      playerPosChanged = true
    } else if (p5.keyboard[83]) {
      position.y += movement.velocityY * time.delta;
      playerPosChanged = true
    }

    if (playerPosChanged === true) {
      position.x = s2d.clamp(position.x, viewport.x1, viewport.x2);
      position.y = s2d.clamp(position.y, viewport.y1, viewport.y2);

      // update the mesh points and normals
      s2d.Shape.updatePolygon(shape.sides, shape.rotation, shape.radius, position, mesh.points);
      s2d.Shape.updateEdgeNormals(mesh.points, mesh.normals);

      // make sure the trail stays with the player shipF
      const trailMap = ecs.getMap(Components.PlayerTrailParticleGenerator);
      const trailEntityId = trailMap.firstKey()
      const emitter = ecs.get(trailEntityId, Components.ShapeParticleEmitter);
      emitter.minMaxStartPos.x1 = position.x - 5
      emitter.minMaxStartPos.x2 = position.x + 5
      emitter.minMaxStartPos.y1 = position.y + 15
      emitter.minMaxStartPos.y2 = position.y + 15
    }
  }

}

