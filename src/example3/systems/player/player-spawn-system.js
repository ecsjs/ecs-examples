import s2d from 'simple-2d'
import BaseSystem from '@shared/systems/base-system.js'
import * as Components from '@example3/components.js';
import { ecs } from 'ecsjs';

export class PlayerSpawnSystem extends BaseSystem {

  onload() {
    this.spawnPlayer(10, 150, 0.15);
  }

  spawnPlayer(x, y, velocity) {
    const playerPos = new Components.Position(x, y),
      // player movement velocity
      playerMov = new Components.Movement(
        //velocityX
        velocity,
        //velocityY,
        velocity,
        //velocityR
        0
      ),
      // player ship shape
      playerShape = new Components.Shape(
        //sides
        3,
        //rotation
        Math.PI,
        //radius
        15,
        //fill
        new s2d.Rgba(0, 0, 0, 1),
        //stroke
        new s2d.Rgba(255, 255, 233, 1)
      ),
      // setup the initial mesh data
      points = s2d.Shape.createPolygon(playerShape.sides, playerShape.rotation, playerShape.radius, playerPos),
      normals = s2d.Shape.createEdgeNormals(points),
      playerShapeMesh = new Components.Mesh(points, normals);

    const entityId = ecs.getNextId()
    ecs.set(entityId, new Components.Player())
    ecs.set(entityId, playerPos)
    ecs.set(entityId, playerMov)
    ecs.set(entityId, playerShape)
    ecs.set(entityId, playerShapeMesh)


    ecs.set(ecs.getNextId(), new Components.PlayerScore())
  }


}