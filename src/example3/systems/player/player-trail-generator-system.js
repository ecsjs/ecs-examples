import { ecs } from 'ecsjs'
import s2d from 'simple-2d'
import { AbstractTimerParticleSystem } from '@shared/systems/abstract-timer-particle-system.js';
import * as Components from '@example3/components.js'
import { ParticleFactory } from '@example3/systems/particles/particle-factory.js'

export class PlayerTrailGeneratorSystem extends AbstractTimerParticleSystem {

  constructor (p5) {
    super(p5, Components.PlayerTrailParticleSpawner)
  }

  onload() {
    this.createTrailGenerator()
  }

  createTrailGenerator() {
    const viewport = this.p5.viewport

    const playerEntityId = ecs.getMap(Components.Player).firstKey()
    const playerPos = ecs.get(playerEntityId, Components.Position)

    const rebirthRate = 50,  // every 20th of a second
      minLife = 200,     // in ms
      maxLife = 400,     // in ms

      // engine component
      timerParticleSpawner = new Components.PlayerTrailParticleSpawner(
        // rebirth rate in ms
        rebirthRate,

        // life time in ms
        minLife, maxLife,
        // elapsedMs
        0
      ),

      // create a shape generator
      shapeEmitter = new Components.ShapeParticleEmitter(
        // minMaxStartPos
        {
          x1: playerPos.x - 5,      // min x
          x2: playerPos.x + 5,      // max x
          y1: playerPos.y + 15,     // min y
          y2: playerPos.y + 15      // max y
        },
        // minMaxVelocity
        {
          x1: -0.025,         // minVelocityX
          x2: 0.025,          // maxVelocityX
          y1: .1,             // minVelocityY
          y2: .2,             // maxVelocityY
          r1: -0.005,         // minVelocityR // rotation min
          r2: 0.005           // maxVelocityR  // rotation max
        },
        1, // minRadius
        3, // maxRadius
        3,  // minSides
        4, // maxSides
        new s2d.Rgba(255, 128, 0, 0.5),       // minFill
        new s2d.Rgba(255, 0, 0, 1),           // maxFill
        new s2d.Rgba(255, 255, 255, .1),     // minStroke
        new s2d.Rgba(255, 255, 255, .2)       // maxStroke
      )

    ecs.set(playerEntityId, new Components.PlayerTrailParticleGenerator())
    ecs.set(playerEntityId, timerParticleSpawner)
    ecs.set(playerEntityId, shapeEmitter)
  }

  onParticleBirth(time, entityId, spawner) {
    this.spawnTrailParticle(
      spawner,
      ecs.get(entityId, Components.ShapeParticleEmitter)
    )
  }

  spawnTrailParticle(timeParticleSpawner, shapeEmitter) {
    // compute the life time
    const timeToLive = Math.round(random(timeParticleSpawner.minLife, timeParticleSpawner.maxLife))

    // create and save the particle entity
    const entityId = ecs.getNextId()
    ecs.set(entityId, new Components.Particle())
    // set the life time
    ecs.set(entityId, new Components.ParticleLifeTime(timeToLive))

    ParticleFactory.spawnShapeParticle(entityId, shapeEmitter)
  }

}