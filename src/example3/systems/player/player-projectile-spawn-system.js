import * as Components from '@example3/components.js';
import { BombFactory } from "@example3/systems/bombs/bomb-factory.js";
import { SoundSystem } from "@example3/systems/sound/sound-system.js";
import BaseSystem from '@shared/systems/base-system.js';
import { ecs } from 'ecsjs';
import { ProjectileFactory } from "../projectiles/projectile-factory.js";

export class PlayerProjectileSpawnSystem extends BaseSystem {

  onframe(time) {
    let p5 = this.p5
    let entityMap = ecs.getMap(Components.Player)
    let [entityId, player] = entityMap.firstEntry() ?? []
    if (!player || player.alive === false) return

    // bullets
    if (player.timeUntilNextBullet === 0) {

      if (p5.mouseIsPressed && p5.mouseButton == p5.LEFT) {
        ProjectileFactory.spawnBullet(entityId, p5.mouseX, p5.mouseY)
        SoundSystem.play('laser', .5)
        player.timeUntilNextBullet = 300
      }

    } else {
      player.timeUntilNextBullet = Math.max(0, player.timeUntilNextBullet - time.delta)
    }

    // bombs
    if (player.timeUntilNextBomb === 0) {

      if (p5.mouseIsPressed && p5.mouseButton == p5.RIGHT) {
        BombFactory.spawnBomb(entityId, p5.mouseX, p5.mouseY)
        SoundSystem.play('bomb', .5)
        player.timeUntilNextBomb = 2000
      }

    } else {

      player.timeUntilNextBomb = Math.max(0, player.timeUntilNextBomb - time.delta)

    }


  }

}