import * as Components from '@example3/components.js'
import { ParticleFactory } from "@example3/systems/particles/particle-factory.js"
import { SoundSystem } from "@example3/systems/sound/sound-system.js"
import BaseSystem from '@shared/systems/base-system.js'
import { ecs } from 'ecsjs'
import s2d from 'simple-2d'

export class PlayerCollisionSystem extends BaseSystem {

  onframe(time) {
    const playerMap = ecs.getMap(Components.Player)
    const [playerEntityId, playerEntity] = playerMap.firstEntry() ?? [];
    if (!playerEntity || playerEntity.alive == false) return;
    const playerShape = ecs.get(playerEntityId, Components.Shape);
    const playerMesh = ecs.get(playerEntityId, Components.Mesh);
    let hasCollided = false;

    //  test against all nodes for a collision
    const playerObstacleMap = ecs.getMap(Components.PlayerObstacle);
    for (let entityId of playerObstacleMap.keys()) {

      //  get the shape's mesh
      const obstacleMesh = ecs.get(entityId, Components.Mesh);

      //  determine the overlap amount
      let mtv = s2d.Shape.getMinimumTranslation(
        //  points1
        playerMesh.points,
        //  points2
        obstacleMesh.points,
        //  axes (combined normals from points1 and points2 )
        playerMesh.normals.concat(obstacleMesh.normals)
      );

      //  if we have an mtv object then we have an overlap between the two shapes
      if (mtv) {
        ParticleFactory.spawnExplosionParticle(entityId, time.elapsed);
        // this.services.statsService.increaseKillScore();
        hasCollided = true;
      }
    }

    if (hasCollided) {
      playerEntity.alive = false;
      // kill the player
      this.explodePlayer(playerEntityId, playerShape, time.elapsed);
    }

  }

  explodePlayer(playerEntityId, shape, elapsedMs) {
    // set time of death
    ecs.getMap(Components.PlayerScore).firstValue().timeDied = elapsedMs

    // add an explosion entity for its system to process
    ecs.set(playerEntityId, new Components.ParticleExplosion(400, elapsedMs))
    // set the fill to black to match background
    shape.fill.zero()
    // play death sound
    SoundSystem.play('playerExplosion')
  }

}