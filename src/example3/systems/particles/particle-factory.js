import * as Components from '@example3/components.js';
import { SoundSystem } from "@example3/systems/sound/sound-system.js";
import { ecs } from 'ecsjs';
import s2d from 'simple-2d';

export class ParticleFactory {

  static spawnShapeParticle(entityId, shapeEmitter) {
    // setup the particle position
    const x = s2d.randomMinMax(shapeEmitter.minMaxStartPos.x1, shapeEmitter.minMaxStartPos.x2),
      y = s2d.randomMinMax(shapeEmitter.minMaxStartPos.y1, shapeEmitter.minMaxStartPos.y2),
      r = s2d.randomMinMax(shapeEmitter.minMaxStartPos.r1, shapeEmitter.minMaxStartPos.r2)

    const shapePos = new Components.Position(x, y, r)

    // setup the particle velocity
    const velocityX = s2d.randomMinMax(shapeEmitter.minMaxVelocity.x1, shapeEmitter.minMaxVelocity.x2),
      velocityY = s2d.randomMinMax(shapeEmitter.minMaxVelocity.y1, shapeEmitter.minMaxVelocity.y2),
      velocityR = s2d.randomMinMax(shapeEmitter.minMaxVelocity.r1, shapeEmitter.minMaxVelocity.r2)

    const shapeMov = new Components.Movement(velocityX, velocityY, velocityR)

    // setup the shape
    const sides = Math.round(s2d.randomMinMax(shapeEmitter.minSides, shapeEmitter.maxSides)),
      rotation = 0,
      radius = s2d.randomMinMax(shapeEmitter.minRadius, shapeEmitter.maxRadius),
      fill = s2d.Colour.randomRgbaMinMax(shapeEmitter.minFill, shapeEmitter.maxFill),
      stroke = s2d.Colour.randomRgbaMinMax(shapeEmitter.minStroke, shapeEmitter.maxStroke)

    const shape = new Components.Shape(sides, rotation, radius, fill, stroke)

    // setup the initial mesh data
    const points = s2d.Shape.createPolygon(shape.sides, shape.rotation, shape.radius, shapePos),
      normals = s2d.Shape.createEdgeNormals(points)

    const mesh = new Components.Mesh(points, normals)

    // connect the components to the particle entity
    ecs.set(entityId, shapePos)
    ecs.set(entityId, shapeMov)
    ecs.set(entityId, shape)
    ecs.set(entityId, mesh)

    // this.statsService.increaseParticleCount();
  }

  static spawnExplosionParticle(entityId, elapsedMs, isBomb) {
    // set the fill to black to match background
    const shape = ecs.get(entityId, Components.Shape)
    shape.fill.zero()

    // remove the obstacle and lifeTime entities so their systems dont try to process them
    ecs.remove(entityId, Components.ParticleLifeTime)
    ecs.remove(entityId, Components.PlayerObstacle)

    // add an explosion entity for its system to process
    if (isBomb) {
      // generate an explosion
      ecs.set(entityId, new Components.BombExplosion(800, elapsedMs))
      shape.stroke.zero()
      shape.stroke.r = 255
      shape.stroke.g = 255
    } else {
      ecs.remove(entityId, Components.ProjectileObstacle)
      ecs.set(entityId, new Components.ParticleExplosion(400, elapsedMs))
    }

    SoundSystem.play('explosion', .5)
  }

}