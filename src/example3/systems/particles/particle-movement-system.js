import * as Components from '@example3/components.js'
import BaseSystem from '@shared/systems/base-system.js'
import { ecs } from 'ecsjs'
import s2d from 'simple-2d'

export class ParticleMovementSystem extends BaseSystem {

  onframe(time) {
    // loop over all the Particle entities
    let entityMap = ecs.getMap(Components.Particle)
    for (const entityId of entityMap.keys()) {
      const shape = ecs.get(entityId, Components.Shape)
      const movement = ecs.get(entityId, Components.Movement)
      const position = ecs.get(entityId, Components.Position)
      if (movement) {
        shape.rotation = (shape.rotation + (movement.velocityR * time.delta)) % s2d.TAU
        position.x += movement.velocityX * time.delta
        position.y += movement.velocityY * time.delta

        const mesh = ecs.get(entityId, Components.Mesh)
        s2d.Shape.updatePolygon(shape.sides, shape.rotation, shape.radius, position, mesh.points)
      }
    }
  }

}