import * as Components from '@example3/components.js'
import BaseSystem from '@shared/systems/base-system.js'
import { ecs } from 'ecsjs'

export class ParticleLifeSystem extends BaseSystem {

  onframe(time) {
    let entityMap = ecs.getMap(Components.ParticleLifeTime)
    for (let [entityId, particleLifeTime] of entityMap.entries()) {
      particleLifeTime.timeToLive -= time.delta
      if (particleLifeTime.timeToLive <= 0) {
        // removes the node and the related node + its entities
        ecs.destroyEntity(entityId)
      }
    }
  }

}