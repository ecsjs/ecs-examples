import * as Components from '@example3/components.js'
import BaseSystem from '@shared/systems/base-system.js'
import { ecs } from 'ecsjs'
import s2d from 'simple-2d'

export class ParticleExplosionSystem extends BaseSystem {

  onframe(time) {
    // loop all the ParticleExplosion entities
    const elapsedMs = time.elapsed
    const particleExplosionMap = ecs.getMap(Components.ParticleExplosion)

    for (const [entityId, explosion] of particleExplosionMap.entries()) {
      const timeLivedMs = elapsedMs - explosion.frameStarted
      if (timeLivedMs >= explosion.maxLife)
        ecs.destroyEntity(entityId)
      else {
        this.updateExplosion(entityId, explosion.maxLife, timeLivedMs)
      }
    }
  }

  updateExplosion(entityId, maxLife, timeLivedMs) {
    // get the components
    const shape = ecs.get(entityId, Components.Shape)
    const shapePos = ecs.get(entityId, Components.Position)
    const mesh = ecs.get(entityId, Components.Mesh)

    // calculate the alpha value
    const divisor = 1 / (maxLife / timeLivedMs)
    shape.stroke.a = (1 - divisor)
    shape.radius += 3
    shape.rotation += 30

    // recalculate the mesh points
    s2d.Shape.updatePolygon(shape.sides, shape.rotation, shape.radius, shapePos, mesh.points)
  }

}