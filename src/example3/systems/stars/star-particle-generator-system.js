import * as Components from '@example3/components.js';
import { ParticleFactory } from '@example3/systems/particles/particle-factory.js';
import { AbstractTimerParticleSystem } from '@shared/systems/abstract-timer-particle-system.js';
import { ecs } from 'ecsjs';
import s2d from 'simple-2d';

export class StarParticleGeneratorSystem extends AbstractTimerParticleSystem {

  constructor (p5) {
    super(p5, Components.StarParticleSpawner);
  }

  onload() {
    this.createStarParticleEmitter();
  }

  createStarParticleEmitter() {
    var viewport = this.p5.viewport,

      // engine component
      timerParticleSpawner = new Components.StarParticleSpawner(
        // rebirth rate in ms
        100,
        // min life time in ms
        4000,
        // max life time in ms
        5000,
        // elapsedMs
        0
      ),

      // create the shape emitter
      shapeEmitter = new Components.ShapeParticleEmitter(
        // minMaxStartPos
        {
          x1: viewport.x1,    // min x
          x2: viewport.x2,    // max x
          y1: viewport.y1,    // min y
          y2: viewport.y1     // max y
        },
        // minMaxVelocity
        {
          x1: 0,      // minVelocityX
          x2: 0,      // maxVelocityX
          y1: .3,     // minVelocityY
          y2: .5,     // maxVelocityY
          r1: 0,      // minVelocityR // rotation min
          r2: 0       // maxVelocityR  // rotation max
        },
        1, // minRadius
        2, // maxRadius
        4, // minSides
        4, // maxSides
        new s2d.Rgba(255, 255, 125, 1), // minFill
        new s2d.Rgba(255, 255, 255, 1),    // maxFill
        new s2d.Rgba(0, 0, 0, 0), // minStroke,
        new s2d.Rgba(0, 0, 0, 0)  // maxStroke
      );

    const entityId = ecs.getNextId()
    ecs.set(entityId, new Components.StarParticleGenerator())
    ecs.set(entityId, timerParticleSpawner)
    ecs.set(entityId, shapeEmitter)
  }

  onParticleBirth(deltaMs, entityId, spawner) {
    this.spawnStarParticle(spawner, ecs.get(entityId, Components.ShapeParticleEmitter))
  }

  spawnStarParticle(timeParticleSpawner, shapeEmitter) {
    // compute the life time
    const timeToLive = Math.round(s2d.randomMinMax(timeParticleSpawner.minLife, timeParticleSpawner.maxLife))

    // create and save the particle node entity
    const entityId = ecs.getNextId()
    ecs.set(entityId, new Components.Particle())
    ecs.set(entityId, new Components.ParticleLifeTime(timeToLive))

    ParticleFactory.spawnShapeParticle(entityId, shapeEmitter);
  }

}