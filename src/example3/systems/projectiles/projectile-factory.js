import * as Components from '@example3/components.js';
import { ecs } from 'ecsjs';
import s2d from 'simple-2d';

export class ProjectileFactory {

  static spawnBullet(entityId, x, y) {
    const playerTopVec = ecs.get(entityId, Components.Mesh).points[0],
      bulletPos = new Components.Position(playerTopVec.x, playerTopVec.y),
      bulletMov = new Components.Movement(0.2, 0.2, 0.0),
      bulletDir = new Components.Direction(0, -1),
      bulletShape = new Components.Shape(
        //sides
        3,
        //rotation
        0,
        //radius
        3,
        //fill
        new s2d.Rgba(255, 255, 255, 1),
        //stroke
        new s2d.Rgba(255, 0, 0, 1)
      ),
      // setup the initial mesh data
      points = s2d.Shape.createPolygon(
        bulletShape.sides,
        bulletShape.rotation,
        bulletShape.radius,
        bulletPos
      ),
      normals = s2d.Shape.createEdgeNormals(points),
      bulletMesh = new Components.Mesh(points, normals);

    const newEntityId = ecs.getNextId()
    ecs.set(newEntityId, new Components.BulletProjectile())
    ecs.set(newEntityId, bulletPos)
    ecs.set(newEntityId, bulletMov)
    ecs.set(newEntityId, bulletDir)
    ecs.set(newEntityId, bulletShape)
    ecs.set(newEntityId, bulletMesh)
    ecs.set(newEntityId, new Components.Projectile())
  }


}