import * as Components from '@example3/components.js';
import BaseSystem from '@shared/systems/base-system.js';
import { ecs } from 'ecsjs';
import s2d from 'simple-2d';

export class ProjectileMovementSystem extends BaseSystem {

  onframe(time) {
    const viewport = this.p5.viewport
    const componentMap = ecs.getMap(Components.Projectile)

    for (const entityId of componentMap.keys()) {
      const movement = ecs.get(entityId, Components.Movement)
      if (movement === undefined) continue

      const position = ecs.get(entityId, Components.Position)
      const direction = ecs.get(entityId, Components.Direction)

      position.x += direction.dirX * (movement.velocityX * time.delta)
      position.y += direction.dirY * (movement.velocityY * time.delta)

      if (viewport.isOutside(position.x, position.y)) {
        ecs.destroyEntity(entityId)
        continue
      }

      const shape = ecs.get(entityId, Components.Shape)
      const mesh = ecs.get(entityId, Components.Mesh)
      s2d.Shape.updatePolygon(
        shape.sides,
        shape.rotation,
        shape.radius,
        position,
        mesh.points
      )

    }

  }

}
