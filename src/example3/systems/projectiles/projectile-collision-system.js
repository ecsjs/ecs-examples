import * as Components from '@example3/components.js'
import { ParticleFactory } from "@example3/systems/particles/particle-factory.js"
import BaseSystem from '@shared/systems/base-system.js'
import { ecs } from 'ecsjs'
import s2d from 'simple-2d'
// import { SoundSystem } from "@example3/systems/sound/sound-system.js"

export class ProjectileCollisionSystem extends BaseSystem {

  onframe(time) {
    let projectileMap = ecs.getMap(Components.Projectile)
    let projectileObstacleMap = ecs.getMap(Components.ProjectileObstacle)

    // loop all projectile entities
    for (let entityId of projectileMap.keys()) {
      let projectileMesh = ecs.get(entityId, Components.Mesh)
      let hasCollided = false

      // test against all collidable entities
      for (let obstacleEntityId of projectileObstacleMap.keys()) {
        //  get the projectileShape's projectileMesh
        let obstacleMesh = ecs.get(obstacleEntityId, Components.Mesh)

        // determine the overlap amount
        let mtv = s2d.Shape.getMinimumTranslation(
          // points1
          projectileMesh.points,
          // points2
          obstacleMesh.points,
          // axes (combined normals from points1 and points2 )
          projectileMesh.normals.concat(obstacleMesh.normals)
        );

        // if we have an mtv object then we have an overlap between the two shapes
        if (mtv) {
          // explode what the projtile has hit
          ParticleFactory.spawnExplosionParticle(
            obstacleEntityId,
            time.elapsed,
            ecs.has(obstacleEntityId, Components.BombProjectile)
          )

          // this.services.statsService.increaseKillScore()
          hasCollided = true
        }
      }

      if (hasCollided) this.projectileCollided(entityId, time.elapsed)
    }

  }

  projectileCollided(entityId, elapsedMs) {
    ecs.getMap(Components.PlayerScore).firstValue().total++
    ecs.destroyEntity(entityId);
  }

}