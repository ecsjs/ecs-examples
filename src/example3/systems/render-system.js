import BaseSystem from '@shared/systems/base-system.js'
import { ecs } from 'ecsjs'
import * as Components from '../components.js'

export class RenderSystem extends BaseSystem {

  constructor (p5) {
    super(p5)
  }

  onframe(time) {
    const p5 = this.p5
    const viewport = this.p5.viewport

    p5.colorMode(p5.RGB, 255, 255, 255, 1);
    p5.blendMode(p5.NORMAL);

    p5.push()

    p5.translate(p5.width * .5, p5.height * .5)

    this.drawParticles(p5)
    this.drawProjectiles(p5)
    this.drawPlayer(p5, viewport, time)

    p5.pop()
  }

  drawProjectiles(p5) {
    this.drawShapesForComponent(p5, Components.BulletProjectile)
    this.drawShapesForComponent(p5, Components.BombProjectile)
  }

  drawParticles(p5) {
    p5.push()
    this.drawShapesForComponent(p5, Components.Particle)
    p5.pop()
  }

  drawPlayer(p5, viewport, time) {
    // get all the ShapeComponents
    const player = ecs.getMap(Components.Player).firstValue()
    const score = ecs.getMap(Components.PlayerScore).firstValue()

    if (player && player.alive) {
      this.drawShapesForComponent(p5, Components.Player)
      this.drawTimePlayed(p5, viewport, time.elapsed)
    } else {
      this.drawTimePlayed(p5, viewport, score.timeDied)
    }

    this.drawPlayerScore(p5, viewport, score)
  }

  drawPlayerScore(p5, viewport, score) {
    fill(255)
    textSize(30)
    const output = score.total.toString() + " kills"
    p5.text(output, viewport.x1 + 10, viewport.y2 - 10)
  }

  drawTimePlayed(p5, viewport, elapsed) {
    fill(255)
    textSize(30)

    const fmt = round(elapsed / 1000)
    const output = fmt.toString() + " sec"
    const xw = textWidth(output)

    p5.text(output, viewport.x2 - xw - 10, viewport.y2 - 10)
  }

  drawShapesForComponent(p5, entityType) {
    // get all the ShapeComponents
    const entityMap = ecs.getMap(entityType)

    for (const entityId of entityMap.keys()) {
      const shape = ecs.get(entityId, Components.Shape)
      const mesh = ecs.get(entityId, Components.Mesh)

      // render the shape to the canvas
      this.drawShape(p5, mesh.points, shape.fill.toString().toLowerCase(), shape.stroke.toString().toLowerCase())
    }
  }

  drawShape(p5, points, fill, stroke) {
    if (fill) p5.fill(fill)
    if (stroke) p5.stroke(stroke)

    p5.beginShape()
    for (let index = 0; index < points.length; index++) {
      const point = points[index]
      p5.vertex(point.x, point.y)
    }
    p5.endShape(p5.CLOSE)
  }

}