import * as Components from '@example3/components.js';
import { ecs } from 'ecsjs';
import s2d from 'simple-2d';

export class BombFactory {

  static spawnBomb(entityId, x, y) {
    var playerTopVec = ecs.get(entityId, Components.Mesh).points[0],
      position = new Components.Position(playerTopVec.x, playerTopVec.y),
      movement = new Components.Movement(0.2, 0.2, 0.0),
      direction = new Components.Direction(0, -1),
      shape = new Components.Shape(
        //sides
        8,
        //rotation
        0,
        //radius
        6,
        //fill
        new s2d.Rgba(0, 0, 0, 0),
        //stroke
        new s2d.Rgba(255, 255, 233, 1)
      ),
      // setup the initial mesh data
      points = s2d.Shape.createPolygon(
        shape.sides,
        shape.rotation,
        shape.radius,
        position
      ),
      normals = s2d.Shape.createEdgeNormals(points),
      mesh = new Components.Mesh(points, normals)

    const newEntityId = ecs.getNextId();
    ecs.set(newEntityId, new Components.BombProjectile())
    ecs.set(newEntityId, position)
    ecs.set(newEntityId, movement)
    ecs.set(newEntityId, direction)
    ecs.set(newEntityId, shape)
    ecs.set(newEntityId, mesh)
    ecs.set(newEntityId, new Components.Projectile())
  }

}