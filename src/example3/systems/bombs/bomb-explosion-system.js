import * as Components from '@example3/components.js'
import BaseSystem from '@shared/systems/base-system.js'
import { ecs } from 'ecsjs'
import s2d from 'simple-2d'

export class BombExplosionSystem extends BaseSystem {

  onframe(time) {
    // loop all the components
    const elapsedMs = time.elapsed
    const explosionMap = ecs.getMap(Components.BombExplosion)

    for (const [entityId, explosion] of explosionMap.entries()) {
      const timeLivedMs = elapsedMs - explosion.frameStarted
      if (timeLivedMs >= explosion.maxLife)
        ecs.destroyEntity(entityId)
      else
        this.expandExplosion(entityId)
    }
  }

  expandExplosion(entityId) {
    // get the component data
    const shape = ecs.get(entityId, Components.Shape)
    const shapePos = ecs.get(entityId, Components.Position)
    const mesh = ecs.get(entityId, Components.Mesh)

    shape.radius += 1
    shape.rotation += 0.05

    // recalculate the mesh points
    s2d.Shape.updatePolygon(shape.sides, shape.rotation, shape.radius, shapePos, mesh.points)
  }

}