import * as Components from '@example3/components.js'
import { ParticleFactory } from '@example3/systems/particles/particle-factory.js'
import BaseSystem from '@shared/systems/base-system.js'
import { ecs } from 'ecsjs'
import s2d from 'simple-2d'
// import { SoundSystem } from "@example3/systems/sound/sound-system.js"

export class BombExplosionCollisionSystem extends BaseSystem {

  onframe(time) {
    const elapsedMs = time.elapsed

    // get all the bombs
    const bombExplosionMap = ecs.getMap(Components.BombExplosion)

    // get all the projectile obstacles
    const obstacleMap = ecs.getMap(Components.ProjectileObstacle)

    for (let entityId of bombExplosionMap.keys()) {
      const projectileMesh = ecs.get(entityId, Components.Mesh)
      let hasCollided = false

      //  test against all components for a collision
      for (let obstacleId of obstacleMap.keys()) {
        //  get the projectileShape's projectileMesh
        const obstacleMesh = ecs.get(obstacleId, Components.Mesh)

        //  determine the overlap amount
        const mtv = s2d.Shape.getMinimumTranslation(
          //  points1
          projectileMesh.points,
          //  points2
          obstacleMesh.points,
          //  axes (combined normals from points1 and points2 )
          projectileMesh.normals.concat(obstacleMesh.normals)
        )

        //  if we have an mtv object then we have an overlap between the two shapes
        if (mtv) {
          ParticleFactory.spawnExplosionParticle(obstacleId, elapsedMs)
          ecs.getMap(Components.PlayerScore).firstValue().total++

          hasCollided = true
        }
      }

    }

  }

}