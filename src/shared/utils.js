export function getViewport(p5) {
  // translate to cartesian coordinates
  const cx = p5.width / 2
  const cy = p5.height / 2

  const width = p5.width
  const height = p5.height
  const x2 = p5.width / 2
  const y2 = p5.height / 2

  return {
    x1: -x2,
    x2,
    y1: -y2,
    y2,
    cx,
    cy,
    width,
    height,
    isOutside: function (x, y) {
      return (x < this.x1 || x > this.x2 || y < this.y1 || y > this.y2)
    }
  }
}

let paused = false
let lastTimestamp = 0.0
let elapsed = 0.0

export function pause(state) {
  if (state === true) {
    paused = true
    noLoop()
  } else {
    // resume the loop
    paused = false
    lastTimestamp = 0.0
    loop()
  }
}

export function getTime(now) {
  let delta = 0.0

  if (lastTimestamp != 0.0)
    // determine the delta since the last recorded frame time
    delta = now - lastTimestamp

  // update last frame time stamp
  lastTimestamp = now

  // update the elapsed ms
  elapsed += delta

  // increase the frames
  // framesElapsed++
  // return the delta
  return { delta, elapsed }
}