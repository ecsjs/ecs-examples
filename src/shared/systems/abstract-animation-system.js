import BaseSystem from './base-system.js';

export class AbstractAnimationSystem extends BaseSystem {

  constructor(p5, animationComponentClass) {
    super(p5);

    this.animationComponentClass = animationComponentClass;
    this.hasFrameHandler = typeof (this.__proto__.animateFrame) == "function";
    this.hasEndedHandler = typeof (this.__proto__.animationEnded) == "function";
  }

  onframe(deltaMs) {
    var animationMap = this.EntityMap.getComponentMap(this.animationComponentClass);

    animationMap.forEach((animationEntity, animationEntityKey) => {
      var keyFrame, nextKeyFrameIndex;
      if (animationEntity.paused === true) {
        return;
      }

      animationEntity.elapsed += deltaMs + 1;

      nextKeyFrameIndex = animationEntity.lastKeyFrameIndex + 1;

      keyFrame = animationEntity.keyFrames[nextKeyFrameIndex];

      if (keyFrame.frameMs <= animationEntity.elapsed) {

        if (this.hasFrameHandler === true) {
          this.animateFrame(animationEntityKey, animationEntity, keyFrame);
        }

        if (nextKeyFrameIndex === (animationEntity.keyFrames.length - 1)) {
          if (this.hasEndedHandler === true) {
            this.animationEnded(animationEntityKey, animationEntity);
          }
          animationEntity.lastKeyFrameIndex = -1;
        } else {
          animationEntity.lastKeyFrameIndex = nextKeyFrameIndex;
        }

      }
    });
  }

}