import { ecs } from 'ecsjs';
import BaseSystem from './base-system.js';

export class AbstractTimerParticleSystem extends BaseSystem {

  constructor (p5, timerParticleSpawnerNodeType) {
    super(p5);

    const hasParticleBirthHandler = typeof (this.__proto__.onParticleBirth) == "function";
    if (hasParticleBirthHandler === false)
      throw new ReferenceError("No onParticleBirth method was found.");

    if (!timerParticleSpawnerNodeType)
      throw new ReferenceError("No timerParticleSpawnerNodeType provided.");

    this.timerParticleSpawnerNodeType = timerParticleSpawnerNodeType;
  }

  onframe(time) {
    const entityMap = ecs.getMap(this.timerParticleSpawnerNodeType);

    for (let [entityId, spawner] of entityMap.entries()) {
      spawner.elapsedMs += time.delta;
      if (spawner.elapsedMs >= spawner.rebirthRate) {
        this.onParticleBirth(time.delta, entityId, spawner);
        spawner.elapsedMs = 0;
      }
    }
  }

}