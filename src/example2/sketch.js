import * as Utils from '@shared/utils.js'
import Systems from './register.js'

window.pause = Utils.pause

window.setup = function () {
  createCanvas(640, 480).parent("cv")

  frameRate(60)
  this.viewport = Utils.getViewport(this)

  // call each system's onload method
  Systems.forEach(system => system.onload && system.onload())
}

window.draw = function () {
  background(0)

  // get the time
  let time = Utils.getTime(millis())

  // run each system's onframe method
  Systems.forEach(s => s.onframe && s.onframe(time))

}