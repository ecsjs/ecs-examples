import { ecs } from 'ecsjs'
import * as Components from './components.js'

// import systems
import { BlockSpawnerSystem } from "./systems/block-spawner-system.js"
import { LightMovementSystem } from "./systems/light-movement-system.js"
import { LightControlSystem } from "./systems/light-control-system.js"
import { LightSpawnerSystem } from "./systems/light-spawner-system.js"
import { RenderSystem } from "./systems/render-system.js"

// register components
ecs.register(...Object.values(Components))

// execution order
export default [
  LightMovementSystem,
  BlockSpawnerSystem,
  LightControlSystem,
  LightSpawnerSystem,
  RenderSystem
].map(s => new s(window))
