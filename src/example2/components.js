import s2d from 'simple-2d'

export class Block { }

export class PointLight {

  constructor(outerRadius, innerRadius, colour, active) {
    this.outerRadius = outerRadius
    this.innerRadius = innerRadius
    this.colour = colour
    this.active = active
  }

}

export class Shape {

  constructor(sides, rotation, radius, fill, stroke) {
    this.sides = sides
    this.rotation = rotation
    this.radius = radius
    this.fill = fill || new s2d.Rgba(0, 0, 0, 0)
    this.stroke = stroke || new s2d.Rgba(0, 0, 0, 0)
  }

}

export class Light {

  constructor(radius, fill) {
    this.radius = radius
    this.fill = fill || new s2d.Rgba(0, 0, 0, 0)
  }

}

export class Mesh {

  constructor(points, normals) {
    this.points = points
    this.normals = normals
  }

}

export class Movement {

  constructor(velocityX, velocityY, velocityR) {
    this.velocityX = velocityX
    this.velocityY = velocityY
    this.velocityR = velocityR
  }

}

export class Position {

  constructor(x, y) {
    this.x = x
    this.y = y
  }

}

export class MouseInput { }
