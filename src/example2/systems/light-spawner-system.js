import s2d from 'simple-2d'
import BaseSystem from '@shared/systems/base-system.js'
import * as Components from '../components.js'
import { ecs } from 'ecsjs'

const MIN_LIGHT_RADIUS = 250
const MAX_LIGHT_RADIUS = 500
const MAX_LIGHT_SPEED = .1

export class LightSpawnerSystem extends BaseSystem {

  onload() {
    const width = this.p5.width * .5
    const height = this.p5.height * .5
    const x1 = -width
    const y1 = -height

    // create some moving lights at start
    for (let num = 0; num < 2; num++) {
      this.spawnPointLight(
        x1 + (Math.random() * width),
        y1 + (Math.random() * height),
        true
      )
    }

    // create mouse controlled light
    this.spawnPointLight(
      x1 + (Math.random() * width),
      y1 + (Math.random() * height),
      false
    )

  }

  spawnPointLight(x, y, active) {
    // set the PositionComponent
    const lightPos = new Components.Position(
      x,
      y
    )

    // setup the MovementComponent
    let velocityX = (Math.random() * (MAX_LIGHT_SPEED * 2)) - MAX_LIGHT_SPEED,
      velocityY = (Math.random() * (MAX_LIGHT_SPEED * 2)) - MAX_LIGHT_SPEED,
      velocityR = 0

    if (velocityX == 0 && velocityY == 0)
      velocityX = velocityY = 1

    const lightMov = new Components.Movement(
      velocityX,
      velocityY,
      velocityR
    )

    // set the PointLightNode
    const outerRadius = Math.max(Math.random() * MAX_LIGHT_RADIUS, MIN_LIGHT_RADIUS)
    const colour = s2d.Colour.randomRgba()

    const pointLight = new Components.PointLight(
      outerRadius,
      outerRadius * 0.1,
      colour,
      active
    )

    // create the entity and add the components to it
    const entityId = ecs.getNextId()
    ecs.set(entityId, pointLight)
    ecs.set(entityId, lightPos)
    if (active)
      ecs.set(entityId, lightMov)
    else {
      ecs.set(entityId, new Components.MouseInput())
    }
  }

}