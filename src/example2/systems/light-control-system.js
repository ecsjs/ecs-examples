import { ecs } from 'ecsjs'
import BaseSystem from '@shared/systems/base-system.js'
import * as Components from '../components.js'

export class LightControlSystem extends BaseSystem {

  onframe(time) {
    const p5 = this.p5

    const { x1, y1, cx, cy, width, height } = p5.viewport
    const mx = p5.mouseX - cx
    const my = p5.mouseY - cy
    const entityMap = ecs.getMap(Components.MouseInput)

    for (let entityId of entityMap.keys()) {
      // get all the Light Positions
      const pointLight = ecs.get(entityId, Components.PointLight)
      const lightPos = ecs.get(entityId, Components.Position)

      if (mx > x1 && mx < width && my > y1 && my < height) {
        lightPos.x = mx
        lightPos.y = my
        pointLight.active = true
      }
      else
        pointLight.active = false

    }
  }
}