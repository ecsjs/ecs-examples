import { ecs } from 'ecsjs'
import s2d from 'simple-2d'
import BaseSystem from '@shared/systems/base-system.js'
import * as Components from '../components.js'

const MIN_BLOCK_SIDES = 3
const MAX_BLOCK_SIDES = 16
const MIN_BLOCK_RADIUS = 10
const MAX_BLOCK_RADIUS = 35

export class BlockSpawnerSystem extends BaseSystem {

  onload() {
    const { x1, y1, width, height } = this.p5.viewport

    // create some cast objects
    for (let num = 0; num < 3; num++) {
      this.spawnBlock(
        x1 + (Math.random() * width),
        y1 + (Math.random() * height)
      )
    }
  }

  spawnBlock(x, y) {
    // setup the PositionComponent
    const shapePos = new Components.Position(
      x,
      y
    )

    // setup the ShapeComponent
    const sides = Math.round(MIN_BLOCK_SIDES + (Math.random() * MAX_BLOCK_SIDES))
    const rotation = Math.random() * s2d.TAU
    const radius = MIN_BLOCK_RADIUS + (Math.random() * MAX_BLOCK_RADIUS)
    const fill = s2d.Colour.randomRgba()
    const stroke = new s2d.Rgba(255, 255, 255, 1)
    const shape = new Components.Shape(
      sides,
      rotation,
      radius,
      fill,
      stroke
    )

    // setup the MeshComponent
    const points = s2d.Shape.createPolygon(shape.sides, shape.rotation, shape.radius, shapePos)

    const normals = s2d.Shape.createEdgeNormals(points)
    const mesh = new Components.Mesh(
      points,
      normals
    )

    // create the entity and add the components to it
    const entityId = ecs.getNextId()
    ecs.set(entityId, new Components.Block())
    ecs.set(entityId, shape)
    ecs.set(entityId, mesh)
    ecs.set(entityId, shapePos)
  }

}