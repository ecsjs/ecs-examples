import { ecs } from 'ecsjs'
import BaseSystem from '@shared/systems/base-system.js'
import * as Components from '../components.js'

export class LightMovementSystem extends BaseSystem {

  onframe(time) {
    const width = this.p5.width * .5
    const height = this.p5.height * .5
    const x1 = -width
    const y1 = -height

    const entityMap = ecs.getMap(Components.PointLight)

    for (let entityId of entityMap.keys()) {

      const lightMov = ecs.get(entityId, Components.Movement)
      if (!lightMov) continue

      const lightPos = ecs.get(entityId, Components.Position)
      if (!lightPos) throw new ReferenceError(`position component not found for lightEntity: ${entityId}`)

      // test if the shape pos has gone over the bounds of the canvas
      if (lightPos.x < x1) {
        lightMov.velocityX = -lightMov.velocityX
        lightPos.x = x1
      } else if (lightPos.x > width) {
        lightMov.velocityX = -lightMov.velocityX
        lightPos.x = width
      }

      if (lightPos.y < y1) {
        lightMov.velocityY = -lightMov.velocityY
        lightPos.y = y1
      } else if (lightPos.y > height) {
        lightMov.velocityY = -lightMov.velocityY
        lightPos.y = height
      }

      // increment the position by the x\y directions
      lightPos.x += lightMov.velocityX * time.delta
      lightPos.y += lightMov.velocityY * time.delta
    }
  }

}

