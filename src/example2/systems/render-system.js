import { ecs } from 'ecsjs'
import s2d from 'simple-2d'
import BaseSystem from '@shared/systems/base-system.js'
import * as Components from '../components.js'

const SHADOW_MAX_ALPHA = 0.5

export class RenderSystem extends BaseSystem {

  onframe(time) {
    const p5 = this.p5
    const context = this.p5.drawingContext

    p5.colorMode(p5.RGB, 1, 1, 1, 1);
    p5.blendMode(p5.NORMAL);

    p5.push()

    p5.translate(p5.width * .5, p5.height * .5)

    this.drawLights(p5)
    this.drawShadows(p5)
    this.drawBlocks(p5)

    p5.pop()
  }

  drawLights(p5) {
    // get all the PointLightNodes
    const pointLightNodes = ecs.getMap(Components.PointLight)
    for (const [entityId, pointLight] of pointLightNodes.entries()) {
      if (pointLight.active) {
        this.drawPointLight(p5, pointLight, ecs.get(entityId, Components.Position))
      }
    }
  }

  drawPointLight(p5, pointLightEntity, lightPos) {
    p5.blendMode(p5.ADD)

    const context = p5.drawingContext

    context.beginPath()
    context.arc(lightPos.x, lightPos.y, pointLightEntity.outerRadius, 0, s2d.TAU)
    const grad = context.createRadialGradient(lightPos.x, lightPos.y, 0, lightPos.x, lightPos.y, pointLightEntity.outerRadius)
    grad.addColorStop(0, "white")
    grad.addColorStop(pointLightEntity.innerRadius / pointLightEntity.outerRadius, pointLightEntity.colour.toString())
    grad.addColorStop(1, 'black')
    context.fillStyle = grad
    context.fill()
    context.closePath()
  }

  drawBlocks(p5) {
    p5.blendMode(p5.NORMAL);
    this.drawLitShapes(p5, Components.Block)
  }

  drawLitShapes(p5, componentClass) {
    // get all the ShapeComponents
    const componentTypes = ecs.getMap(componentClass)

    for (let entityId of componentTypes.keys()) {
      const shape = ecs.get(entityId, Components.Shape)
      const shapePos = ecs.get(entityId, Components.Position)
      const mesh = ecs.get(entityId, Components.Mesh)

      const fill = this.getColourFromLight(shape.radius, shapePos, shape.fill).toString()
      const stroke = this.getColourFromLight(shape.radius, shapePos, shape.stroke).toString()

      // render the shape to the canvas
      this.drawShape(p5, mesh, fill, stroke)
    }
  }

  drawShadows(p5) {
    p5.blendMode(p5.BLEND);

    // get all the PointLightNodes
    const pointLightNodes = ecs.getMap(Components.PointLight)
    const shadowNodes = ecs.getMap(Components.Block)

    // loop the shadow components
    for (let entityId of shadowNodes.keys()) {
      const shape = ecs.get(entityId, Components.Shape)
      if (shape === undefined) continue

      const shapePos = ecs.get(entityId, Components.Position)
      const shapeMesh = ecs.get(entityId, Components.Mesh)

      // for each point light draw it's shadow
      for (let [entityId, pointLight] of pointLightNodes.entries()) {
        if (pointLight.active) {
          const pointLightPos = ecs.get(entityId, Components.Position)
          if (s2d.Shape.pointInCircle(pointLightPos, pointLight.outerRadius + shape.radius, shapePos) >= 0)
            this.drawShadow(p5, shapeMesh, shape, shapePos, pointLight, pointLightPos)
        }
      }
    }
  }

  drawShadow(p5, shapeMesh, shape, shapePos, light, lightPos) {
    const points = shapeMesh.points
    const edgeDots = s2d.Shape.getEdgeDots(points, lightPos)
    const projectedPoints = []
    let boundaryI1 = -1
    let boundaryI2 = -1
    let shadBoundaryI1 = -1
    let shadBoundaryI2 = -1
    let shouldAdd = false

    // find the boundary points
    // project the points that are connected to an edge that is back facing
    let nextIndex = 0
    let index = edgeDots.length - 1

    for (; nextIndex < edgeDots.length; nextIndex++) {
      const dot = edgeDots[index]
      const nextDot = edgeDots[nextIndex]

      // determine boundary points and store for drawing with later
      if (dot < 0 || nextDot < 0) {
        if (dot < 0 && nextDot > 0) {
          boundaryI1 = index
          shadBoundaryI1 = projectedPoints.length
        } else if (dot > 0 && nextDot < 0) {
          boundaryI2 = index
          shadBoundaryI2 = projectedPoints.length
        }
        shouldAdd = true
      } else
        shouldAdd = false

      if (shouldAdd) {
        // project the point to the radius of the light
        projectedPoints.push(this.projectLight(lightPos, points[index]))
      }

      index = nextIndex
    }

    const shapeToLightdist = s2d.Vec2.distance(shapePos, lightPos)
    let testRadius = light.outerRadius + shape.radius
    testRadius *= testRadius
    const shadowAlpha = SHADOW_MAX_ALPHA - (SHADOW_MAX_ALPHA / (testRadius / shapeToLightdist))

    p5.noStroke()
    p5.fill(0, shadowAlpha)

    // draw the shadow shape
    p5.beginShape()
    if (boundaryI1 > -1 && boundaryI2 > -1) {
      const boundary1 = points[boundaryI1]
      const lightBoundary1 = projectedPoints[shadBoundaryI1]
      const boundary2 = points[boundaryI2]
      const lightBoundary2 = projectedPoints[shadBoundaryI2]

      p5.vertex(boundary1.x, boundary1.y)
      p5.vertex(lightBoundary1.x, lightBoundary1.y)
      p5.vertex(lightBoundary2.x, lightBoundary2.y)
      p5.vertex(boundary2.x, boundary2.y)
    }
    p5.endShape()

    // draw the shape projection (caps the shadow)
    p5.beginShape()
    for (index = 0; index < projectedPoints.length; index++) {
      p5.vertex(projectedPoints[index].x, projectedPoints[index].y)
    }
    p5.endShape(p5.CLOSE)
  }

  projectLight(source, pointToProject) {
    var diff = s2d.Vec2.sub(pointToProject, source)
    return s2d.Vec2.add(pointToProject, diff)
  }

  getColourFromLight(shapeRadius, shapePos, rgbaColour) {
    const intensities = []

    // loop the PointLightNodes
    const pointLightNodes = ecs.getMap(Components.PointLight)
    for (const [entityId, pointLightNode] of pointLightNodes.entries()) {
      if (pointLightNode.active) {
        const lightPos = ecs.get(entityId, Components.Position)
        const distance = s2d.Vec2.distance(lightPos, shapePos)
        let testRadius = pointLightNode.outerRadius + shapeRadius

        testRadius *= testRadius
        if (distance <= testRadius) intensities.push(1 - (distance / testRadius))
      }
    }

    let r = 0, g = 0, b = 0
    for (const intensity of intensities) {
      r += Math.floor(rgbaColour.r * intensity)
      g += Math.floor(rgbaColour.g * intensity)
      b += Math.floor(rgbaColour.b * intensity)
    }

    return new s2d.Rgba(
      r, g, b,
      rgbaColour.a
    )

  }

  drawShape(p5, mesh, fill, stroke) {
    const points = mesh.points

    if (fill) p5.fill(fill.toString().toLowerCase())
    if (stroke) p5.stroke(stroke.toString().toLowerCase())

    p5.beginShape()
    for (let index = 0; index < points.length; index++) {
      const point = points[index]
      p5.vertex(point.x, point.y)
    }
    p5.endShape(p5.CLOSE)
  }

}