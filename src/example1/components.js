import s2d from 'simple-2d'

export class Ufo { }

export class Mesh {
  constructor (points, normals) {
    this.points = points
    this.normals = normals
  }
}

export class Velocity {
  constructor (velocityX, velocityY, velocityR, restitution) {
    this.x = velocityX
    this.y = velocityY
    this.velocityR = velocityR
    this.restitution = restitution
  }
}

export class Position {
  constructor (x, y) {
    this.x = x
    this.y = y
  }
}

export class Shape {
  constructor (sides, rotation, radius, fill, stroke) {
    this.sides = sides
    this.rotation = rotation
    this.radius = radius
    this.fill = fill || new s2d.Rgba(0, 0, 0, 0)
    this.stroke = stroke || new s2d.Rgba(0, 0, 0, 0) //  rgba type
  }
}

export class UfoActive { }

export class UfoCollider {
  constructor () {
    this.canExplode = false
    this.canBounce = false
  }
}

export class UfoExplosion {
  constructor (frameStarted) {
    this.frameStarted = frameStarted //  current frame when the explosion started
  }
}