import { ecs } from 'ecsjs'
import s2d from 'simple-2d'
import BaseSystem from '@shared/systems/base-system.js'
import * as Components from '../components.js'

export class UfoMovementSystem extends BaseSystem {

  onframe(time) {
    const { viewport } = this.p5

    // loop all the ufo entities
    const ufoActiveMap = ecs.getMap(Components.UfoActive);
    for (let entityId of ufoActiveMap.keys()) {
      // get all the associated components
      const shape = ecs.get(entityId, Components.Shape)
      const shapeVel = ecs.get(entityId, Components.Velocity)
      const shapePos = ecs.get(entityId, Components.Position)
      const shapeMesh = ecs.get(entityId, Components.Mesh)

      // make sure the shape is kept in the view space
      this.clampToRect(viewport, shape.radius, shapePos, shapeVel);

      // increment the position by the x\y directions
      shape.rotation = (shape.rotation + (shapeVel.velocityR * time.delta)) % s2d.TAU;
      shapePos.x += (shapeVel.x * time.delta);
      shapePos.y += (shapeVel.y * time.delta);

      // update the mesh location and normals
      s2d.Shape.updatePolygon(shape.sides, shape.rotation, shape.radius, shapePos, shapeMesh.points);
      s2d.Shape.updateEdgeNormals(shapeMesh.points, shapeMesh.normals);
    }

  }

  clampToRect(rect, shapeRadius, shapePos, shapeVel) {
    // test if the shape pos has gone over the bounds of the canvas
    if (shapePos.x < (rect.x1 + shapeRadius)) {
      shapeVel.x *= shapeVel.restitution;
      shapePos.x = rect.x1 + shapeRadius;
      shapeVel.velocityR *= shapeVel.restitution;
    } else if ((shapePos.x + shapeRadius) > rect.x2) {
      shapeVel.x *= shapeVel.restitution;
      shapePos.x = rect.x2 - shapeRadius;
      shapeVel.velocityR *= shapeVel.restitution;
    }

    if (shapePos.y < (rect.y1 + shapeRadius)) {
      shapeVel.y *= shapeVel.restitution;
      shapePos.y = rect.y1 + shapeRadius;
      shapeVel.velocityR *= shapeVel.restitution;
    } else if ((shapePos.y + shapeRadius) > rect.y2) {
      shapeVel.y *= shapeVel.restitution;
      shapePos.y = rect.y2 - shapeRadius;
      shapeVel.velocityR *= shapeVel.restitution;
    }
  }

}





