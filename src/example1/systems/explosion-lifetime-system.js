import { ecs } from 'ecsjs'
import BaseSystem from '@shared/systems/base-system.js'
import * as Components from '../components.js'
import { UfoSpawnerSystem } from './ufo-spawner-system.js'

var MAX_LIFE = 1200; // currently in ms

export class ExplosionLifeTimeSystem extends BaseSystem {

  onframe(time) {
    const { viewport } = this.p5
    const explosionMap = ecs.getMap(Components.UfoExplosion)

    // loop all explosion entities
    for (let [entityId, explosion] of explosionMap.entries()) {
      const timeLivedMs = time.elapsed - explosion.frameStarted

      if (timeLivedMs >= MAX_LIFE) {
        // delete current ufo entity and any associated entities
        ecs.destroyEntity(entityId)

        // respawn a new entity
        UfoSpawnerSystem.SpawnUfo(
          viewport.x1 + (Math.random() * viewport.width),
          viewport.y1 + (Math.random() * viewport.height)
        )

      }

    }

  }

}