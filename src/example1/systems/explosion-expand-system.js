import { ecs } from 'ecsjs'
import s2d from 'simple-2d'
import BaseSystem from '@shared/systems/base-system.js'
import * as Components from '../components.js'

var MAX_LIFE = 1200; // currently in ms

export class ExplosionExpandSystem extends BaseSystem {

  onframe(time) {
    const explosionMap = ecs.getMap(Components.UfoExplosion)

    // loop all explosion entities
    for (let [entityId, explosion] of explosionMap.entries()) {
      const timeLivedMs = time.elapsed - explosion.frameStarted

      const shape = ecs.get(entityId, Components.Shape)
      const shapePos = ecs.get(entityId, Components.Position)
      const shapeMesh = ecs.get(entityId, Components.Mesh)

      // calculate the alpha value
      var divisor = 1 / (MAX_LIFE / timeLivedMs)
      shape.stroke.a = 1 - divisor
      shape.radius += 1

      // recalculate the mesh points
      s2d.Shape.updatePolygon(shape.sides, shape.rotation, shape.radius, shapePos, shapeMesh.points);
    }

  }

}