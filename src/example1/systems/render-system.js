import { ecs } from 'ecsjs'
import BaseSystem from '@shared/systems/base-system.js'
import * as Components from '../components.js';

export class RenderSystem extends BaseSystem {

  onframe(time) {
    const p5 = this.p5
    p5.colorMode(p5.RGB, 255, 255, 255, 1)
    p5.blendMode(p5.NORMAL)

    p5.push()
    p5.translate(p5.width * .5, p5.height * .5)
    this.drawUfoExplosions(p5)
    this.drawUfoActive(p5)
    p5.pop()

  }

  drawUfoExplosions(p5) {
    let ufoExplosionMap = ecs.getMap(Components.UfoExplosion);
    for (let ufoExplosionId of ufoExplosionMap.keys()) {
      this.drawShapeEntity(p5, ufoExplosionId)
    }
  }

  drawUfoActive(p5) {
    let activeUfoMap = ecs.getMap(Components.UfoActive);
    for (let activeUfoId of activeUfoMap.keys()) {
      this.drawShapeEntity(p5, activeUfoId)
    }
  }

  drawShapeEntity(p5, entityId) {
    const shape = ecs.get(entityId, Components.Shape)
    const mesh = ecs.get(entityId, Components.Mesh)
    // render the shape to the canvas
    this.drawShape(p5, mesh.points, shape.fill.toString().toLowerCase(), shape.stroke.toString().toLowerCase())
  }

  drawShape(p5, points, fill, stroke) {
    if (fill) p5.fill(fill)
    if (stroke) p5.stroke(stroke)

    p5.beginShape()
    for (let index = 0; index < points.length; index++) {
      const point = points[index]
      p5.vertex(point.x, point.y)
    }
    p5.endShape(p5.CLOSE)
  }

}