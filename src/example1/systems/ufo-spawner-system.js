import { ecs } from 'ecsjs'
import s2d from 'simple-2d'
import BaseSystem from '@shared/systems/base-system.js'
import * as Components from '../components.js'

const MIN_UFO_SIDES = 3
const MAX_UFO_SIDES = 16
const MIN_UFO_RADIUS = 10
const MAX_UFO_RADIUS = 35
const MAX_UFO_SPEED = .2

export class UfoSpawnerSystem extends BaseSystem {

  onload() {
    const { viewport } = this.p5
    // create a bunch of ufo's on load
    for (let num = 0; num < 10; num++) {
      UfoSpawnerSystem.SpawnUfo(
        viewport.x1 + (Math.random() * viewport.width),
        viewport.y1 + (Math.random() * viewport.height)
      )
    }
  }

  static SpawnUfo(x, y) {
    // setup the position component
    const shapePos = new Components.Position(x, y)

    // setup the shape component
    const shape = new Components.Shape(
      // sides
      Math.round(MIN_UFO_SIDES + (Math.random() * MAX_UFO_SIDES)),
      // rotation
      0,//Math.random() * s2d.TAU,
      // radius
      Math.round(MIN_UFO_RADIUS + (Math.random() * MAX_UFO_RADIUS)),
      // fill
      s2d.Colour.randomRgba(),
      //stroke
      new s2d.Rgba(0, 0, 0, 0)
    )

    // setup the mesh component
    const points = s2d.Shape.createPolygon(shape.sides, shape.rotation, shape.radius, shapePos)
    const normals = s2d.Shape.createEdgeNormals(points)
    const mesh = new Components.Mesh(points, normals)

    // setup movement component
    const restitution = -0.8

    const shapeVel = new Components.Velocity(
      // velocityX
      (Math.random() * (MAX_UFO_SPEED * 2)) - MAX_UFO_SPEED,
      // velocityY
      (Math.random() * (MAX_UFO_SPEED * 2)) - MAX_UFO_SPEED,
      // velocityR
      ((Math.random() * (MAX_UFO_SPEED * 2)) - MAX_UFO_SPEED) * 0.05,
      restitution
    )

    //  create the entity and add the components to it
    const entityId = ecs.getNextId()
    ecs.set(entityId, new Components.Ufo())
    ecs.set(entityId, shape)
    ecs.set(entityId, shapePos)
    ecs.set(entityId, shapeVel)
    ecs.set(entityId, mesh)
    ecs.set(entityId, new Components.UfoActive())
    ecs.set(entityId, new Components.UfoCollider())
  }

}
