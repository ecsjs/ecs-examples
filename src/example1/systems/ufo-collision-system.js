import { ecs } from 'ecsjs'
import s2d from 'simple-2d'
import BaseSystem from '@shared/systems/base-system.js'
import * as Components from '../components.js'

export class UfoCollisionSystem extends BaseSystem {

  onframe(time) {
    // loop all the Collidable entities
    const colliderNodeMap = ecs.getMap(Components.UfoCollider)

    for (let colliderId of colliderNodeMap.keys()) {
      const shapeVel = ecs.get(colliderId, Components.Velocity)
      const shapePos = ecs.get(colliderId, Components.Position)
      const shapeMesh = ecs.get(colliderId, Components.Mesh)
      let hasCollided = false

      //  test against all components for a collision
      for (let testColliderId of colliderNodeMap.keys()) {
        //  check we're not looking at the same test entity
        if (colliderId == testColliderId) continue

        //  get the shape's mesh
        // const testUfo = testCollider.get("source")
        const testMesh = ecs.get(testColliderId, Components.Mesh)

        //  determine the overlap amount
        const mtv = s2d.Shape.getMinimumTranslation(
          //  points1
          shapeMesh.points,
          //  points2
          testMesh.points,
          //  axes (combined normals from points1 and points2 )
          shapeMesh.normals.concat(testMesh.normals)
        )

        //  if we have an mtv object then we have an overlap between the two shapes
        if (mtv) {
          let dx = mtv.normal.x * mtv.overlap
          let dy = mtv.normal.y * mtv.overlap

          if ((dx < 0 && shapeVel.x < 0) || (dx > 0 && shapeVel.x > 0)) {
            dx *= shapeVel.restitution
          }

          if ((dy < 0 && shapeVel.y < 0) || (dy > 0 && shapeVel.y > 0)) {
            dy *= shapeVel.restitution
          }

          shapePos.x += dx
          shapePos.y += dy

          //  get the test shape
          var testShape = ecs.get(testColliderId, Components.Shape)
          this.explodeCollidedComponents(testColliderId, testShape, time.elapsed)
          hasCollided = true
        }
      }

      if (hasCollided) {
        const shape = ecs.get(colliderId, Components.Shape)
        s2d.Shape.updatePolygon(shape.sides, shape.rotation, shape.radius, shapePos, shapeMesh.points)
        s2d.Shape.updateEdgeNormals(shapeMesh.points, shapeMesh.normals)
        this.explodeCollidedComponents(colliderId, shape, time.elapsed);
      }
    }

  }

  explodeCollidedComponents(entityId, shape, elapsed) {
    //  remove the ufo collider to stop this system processing this entity
    ecs.remove(entityId, Components.UfoCollider);
    ecs.remove(entityId, Components.UfoActive);

    shape.fill.zero();
    shape.stroke.r = 255;
    shape.stroke.g = 0;
    shape.stroke.b = 0;
    shape.stroke.a = 1;

    //  add an explosion node to the entity so the explosion system will process it
    ecs.set(entityId, new Components.UfoExplosion(elapsed))
  }

}