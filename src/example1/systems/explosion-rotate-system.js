import { ecs } from 'ecsjs'
import s2d from 'simple-2d'
import BaseSystem from '@shared/systems/base-system.js'
import * as Components from '../components.js'

export class ExplosionRotateSystem extends BaseSystem {

  onframe(time) {
    const explosionMap = ecs.getMap(Components.UfoExplosion)

    // loop all explosion entities
    for (let entityId of explosionMap.keys()) {
      const shape = ecs.get(entityId, Components.Shape)
      const shapeVel = ecs.get(entityId, Components.Velocity)
      const shapePos = ecs.get(entityId, Components.Position)
      const shapeMesh = ecs.get(entityId, Components.Mesh)

      // increment the position by the x\y directions
      shape.rotation = (shape.rotation + (shapeVel.velocityR * time.delta)) % s2d.TAU;

      // update the mesh location and normals
      s2d.Shape.updatePolygon(shape.sides, shape.rotation, shape.radius, shapePos, shapeMesh.points);
      s2d.Shape.updateEdgeNormals(shapeMesh.points, shapeMesh.normals);
    }
  }

}