import { ecs } from 'ecsjs'
import * as Components from './components.js'

// setup systems
import { RenderSystem } from "@example1/systems/render-system.js"

import { UfoSpawnerSystem } from "@example1/systems/ufo-spawner-system.js"
import { UfoCollisionSystem } from "@example1/systems/ufo-collision-system.js"
import { UfoMovementSystem } from "@example1/systems/ufo-movement-system.js"

import { ExplosionLifeTimeSystem } from "@example1/systems/explosion-lifetime-system.js"
import { ExplosionRotateSystem } from "@example1/systems/explosion-rotate-system.js"
import { ExplosionExpandSystem } from "@example1/systems/explosion-expand-system.js"

// register entities
ecs.register(...Object.values(Components))

// execution order
export default [
  UfoSpawnerSystem,
  UfoMovementSystem,
  UfoCollisionSystem,
  ExplosionLifeTimeSystem,
  ExplosionRotateSystem,
  ExplosionExpandSystem,
  RenderSystem
].map(s => new s(window))
