import esbuild from 'esbuild';
import { resolve } from 'node:path';

const projectPath = process.cwd();
const sourcePath = resolve(projectPath, 'src');
const distPath = resolve(projectPath, 'public', 'app');
const isDevEnv = process.env?.BUNDLE_DEV;
const sourceEntryPoint = [
  resolve(sourcePath, 'example1', 'sketch.js'),
  resolve(sourcePath, 'example2', 'sketch.js'),
  resolve(sourcePath, 'example3', 'sketch.js')
];
const outputFile = 'ecs-examples';
const minify = !isDevEnv;

await esbuild.build({
  entryPoints: sourceEntryPoint,
  outdir: distPath,
  // outfile: resolve(distPath, outputFile),
  platform: 'node',
  format: 'esm',
  mainFields: ['module'],
  sourcemap: 'linked',
  bundle: true,
  minify
});